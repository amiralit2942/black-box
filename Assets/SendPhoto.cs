﻿using System.Collections;
using System.Collections.Generic;
using _Scripts.Managers;
using UnityEngine;
using Utils;


public class SendPhoto : MonoBehaviourSingleton<SendPhoto>
{
    private Sprite _selectedSprite;
    public Sprite SelectedSprite => _selectedSprite;

    public void SelectPhoto()
    {
        if (NativeGallery.IsMediaPickerBusy())
        {
            return;
        }

        ChatManager.Instance.wantToSendPic = true;
        NativeGallery.GetImageFromGallery((path) =>
        {
            Texture2D texture = NativeGallery.LoadImageAtPath(path);
            if (texture == null)
            {
                print("Cant Load The Image");
                    return ;
            }

            _selectedSprite = Sprite.Create(texture, new Rect(0, 0, texture.width, texture.height), Vector2.zero);
            
        });
        
        if (_selectedSprite == null)
        {
            //TODO Set Not Found Pic
            return;
        }
        
        ChatManager.Instance.PicPanel(_selectedSprite);
    }

    public void PicSent()
    {
        _selectedSprite = null;
        ChatManager.Instance.wantToSendPic = false;
    }
}
