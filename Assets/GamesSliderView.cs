using System;
using System.Collections;
using System.Collections.Generic;
using _Scripts.Controller.Menu;
using _Scripts.Managers;
using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;
using Utils;

[Serializable]

public class GamesSliderView : MonoBehaviourSingleton<GamesSliderView>
{
    [SerializeField] private Transform _buttonParent;
    private List<ButtonAndContent> _buttons;
    [SerializeField] private GameObject contentMid;
    [SerializeField] private Transform content;
    [SerializeField] private float movingDuration;
    [SerializeField] private ButtonAndContent firstButtonAndContent;
    private List<Transform> _panels;
    [SerializeField] private float _threshold = 3;
    private void OnEnable()
    {
        _panels = new List<Transform>();
        _buttons = new List<ButtonAndContent>();
        for (int i = 0; i < content.childCount; i++)
        {
            _panels.Add(content.GetChild(i));
        }
        for (int i = 0; i < _buttonParent.childCount; i++)
        {
            _buttons.Add(_buttonParent.GetChild(i).GetComponent<ButtonAndContent>());
        }
        firstButtonAndContent.Button.interactable = false;
        _lastClick = firstButtonAndContent;
        _lastClick.transform.localScale = new Vector3(1.3f, 1.3f, 1);
    }

    public void SetChildToCenter(Vector2 delta)
    {
        // float bestPosition = 500000;
        // int bestIndex = 0;
        // for (int i = 0; i < _panels.Count; i++)
        // {
        //     var panel = _panels[i];
        //     var diff = Mathf.Abs(contentMid.transform.position.x - panel.position.x);
        //     print(diff);
        //     if (diff < bestPosition)
        //     {
        //         print(i);
        //         bestPosition = diff;
        //         bestIndex = i;
        //     }
        // }
        // print(bestIndex);
        // ButtonClick(_buttons[bestIndex]);
        var lastIndex = _buttons.FindIndex(t => t.GetInstanceID() == _lastClick.GetInstanceID());
        print(delta);
        if (Mathf.Abs(delta.x) > _threshold)
        {
            if (delta.x < 0)
            {
                if (lastIndex < _buttons.Count - 1)
                {
                    lastIndex++;
                }
            }
            else
            {
                if (lastIndex > 0)
                {
                    lastIndex--;
                }
            }
        }
        ButtonClick(_buttons[lastIndex]);

    }

    private IEnumerator SnapRoutine(Transform target)
    {
        var vDiff = new Vector3(contentMid.transform.position.x - target.position.x, 0);
        var t = 0f;
        var initialPos = content.transform.position;
        var targetPos = initialPos + vDiff;
        while (t < movingDuration)
        {
            t += Time.deltaTime;
            t = Mathf.Clamp(t, 0, movingDuration);
            content.transform.position = Vector3.Lerp(initialPos, targetPos, (t / movingDuration));
            yield return null;
        }
    }

    public void Snap(Transform target)
    {
        StartCoroutine(SnapRoutine(target));
    }

    private ButtonAndContent _lastClick;
    public void ButtonClick(ButtonAndContent buttonAndContent)
    {
        if (_lastClick != null && _lastClick != buttonAndContent)
        {
            _lastClick.Button.interactable = true;
            _lastClick.transform.DOScale(new Vector3(1, 1, 1), .5f);

        }
        Snap(buttonAndContent.Content);
        buttonAndContent.Button.interactable = false;
        _lastClick = buttonAndContent;
        _lastClick.transform.DOScale(new Vector3(1.3f, 1.3f, 1), .2f);
    }
    
    
}
