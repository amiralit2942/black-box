﻿using System;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;

public class DoTweenTest : MonoBehaviour
{
    private RectTransform _rectTransform;
    
    private void Start()
    {
        _rectTransform = GetComponent<RectTransform>();
    }


    public void Test()
    {
        _rectTransform.DOComplete();
        _rectTransform.DOSizeDelta(new Vector2(0,10), 1);
    }
}
