﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using _Scripts.Model;
using _Scripts.Model.ScriptableObjectsSc;
using DG.Tweening;
using UnityEngine;
using Utils;

namespace _Scripts.Managers
{
    public class BotManager : MonoBehaviourSingleton<BotManager>
    {
        public bool isCallingBot;
        private List<BotMessage> _upcomingMessages;
        private BotMessage _nextMessage;
        private List<string> _sentence;
        private bool _botIsBusy;
                 
        private void Start()
        {
            _upcomingMessages = new List<BotMessage>();
            _sentence = new List<string>();
            isCallingBot = false;
            PassNextStateAnswers();
            PassNextMessage();
        }

        
        public void ChekAnswer(string answer)
        {
            isCallingBot = false;
            
            if (_botIsBusy)
            {
                var message =
                    PrefabHolder.Instance.GetRandomHandyResponse(HandyResponseType.DontDisturb);
                ChatManager.Instance.Message(message,MessageSender.Character);
                return;
            }

            _sentence = System.Text.RegularExpressions.Regex.Split( answer, @"\s{1,}").ToList();
            
            // var answers = PrefabHolder.Instance.GetAnswers();
            // var correctAnswers = 0;
            //
            //
            // for (int i = 0; i < answers.Count; i++)
            // {
            //     if (_sentence.Contains(answers[i].Word))
            //     {
            //         correctAnswers++;
            //     }
            // }
            //
            // if (correctAnswers >= answers.Count)
            // {
            //     GameManager.Instance.NextState();
            //     return;
            // }

            if (AnswerCheckingManager.Instance.CheckAnswer(_sentence))
            {
                GameManager.Instance.NextState();
                return;
            }
            
            WrongAnswer();
        }
        
        public void WrongAnswer()
        {
            ChatManager.Instance.Message
                (PrefabHolder.Instance.
                GetRandomHandyResponse(HandyResponseType.WrongAnswer),MessageSender.Character);
        }

        public void IsCallingBot()
        {
            isCallingBot = true;
        }

        public void PassNextStateAnswers()
        {
            _upcomingMessages = PrefabHolder.Instance.PassMessages().ConvertAll(x=>x);
        }

        public void PassNextMessage()
        {
            if (_upcomingMessages == null || _upcomingMessages.Count <= 0)
            {
                print("No Messages Left");
                return;
            }

            var message = _upcomingMessages.OrderBy(x => x.Order);
            _nextMessage = message.First();
            ChatManager.Instance.BotMessage(_nextMessage);
            _botIsBusy = _nextMessage.BotIsBusy;
            _upcomingMessages?.Remove(_nextMessage);
            _nextMessage = null;
        }
        
        //Just for Test
        [SerializeField] private PlayersAnswersHolder answersHolder;

        // public bool ConvertAnswer(string givenAnswer)
        // {
        //     answersHolder.Answers.a
        // }
        

    }
}