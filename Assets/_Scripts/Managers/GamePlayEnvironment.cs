﻿using _Scripts.Model;
using UnityEngine;

namespace _Scripts.Managers
{
    public class GamePlayEnvironment : MonoBehaviour
    {


        public void GetBackGround()
        {
            var sprite = PrefabHolder.Instance.GetBackGroundSprite(); 
            if (sprite != null)
            {
                GameManager.Instance.backGround.sprite = sprite;
            }
        }
    }
}
