using System;
using _Scripts.Utils.Animating_Utils;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace _Scripts.Managers
{
    public class NoticePopUpManager : MonoBehaviour
    {
        [SerializeField] private AnimatedPanel animatedPanel;
        [SerializeField] private GameObject back;
        [SerializeField] private Button acceptButton;
        [SerializeField] private Button rejectButton;
        [SerializeField] private TMP_Text content;

        private bool _isPopUpActive;
        public bool IsPopUpActive => _isPopUpActive;

        public void Active()
        {
            back.gameObject.SetActive(true);
            animatedPanel.Animate();
        }

        public void DeActive()
        {
            back.gameObject.SetActive(false);
            animatedPanel.Animate(true);
        }

        public NoticePopUpManager NewPopUp()
        {
            Active();
            content.gameObject.SetActive(false);
            acceptButton.gameObject.SetActive(false);
            rejectButton.gameObject.SetActive(false);
            acceptButton.onClick.RemoveAllListeners();
            rejectButton.onClick.RemoveAllListeners();
            acceptButton.onClick.AddListener(DeActive);
            rejectButton.onClick.AddListener(DeActive);
            return this;
        }

        public NoticePopUpManager WithContent(string content)
        {
            this.content.gameObject.SetActive(true);
            this.content.text = content;
            return this;
        }

        public NoticePopUpManager WithAcceptButton(Action onClick)
        {
            acceptButton.gameObject.SetActive(true);
            acceptButton.onClick.AddListener(() =>
            {
                if (onClick != null) onClick();
            });
            return this;
        }
        public NoticePopUpManager WithRejectButton(Action onClick = null)
        {
            rejectButton.gameObject.SetActive(true);
            rejectButton.onClick.AddListener(() =>
            {
                if (onClick != null) onClick();
            });
            return this;
        }
        
    }
}