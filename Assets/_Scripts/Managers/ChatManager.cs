﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using _Scripts.Model;
using _Scripts.Model.ScriptableObjectsSc;
using DG.Tweening;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.UIElements;
using Utils;
using Image = UnityEngine.UI.Image;

namespace _Scripts.Managers
{
   
    public class ChatManager : MonoBehaviourSingleton<ChatManager>
    {
        [SerializeField] private Transform chatHolder;
        [SerializeField] public TMP_InputField inputField;
        [SerializeField] private GameObject picPanel;
        [SerializeField] private Image image;
        [SerializeField] private ScrollRect chatScrollView;
        [SerializeField] private VerticalLayoutGroup vlg;
        
        private Sprite _selectedPic;
        
        public bool wantToSendPic;
        private void Start()
        {
            picPanel.SetActive(false);
        }

        private void Update()
        {
            if ( Input.GetKeyDown(KeyCode.KeypadEnter) || Input.GetKeyDown(KeyCode.Return))
            {
                Message(inputField.text,MessageSender.Player);
            }
        }

        public async void Message(string message,MessageSender messageSender = MessageSender.Player)
        {
            if (inputField.text.Length != 0 && inputField.text.Any(x => x != ' '))
            {
                var bubble = Instantiate(PrefabHolder.Instance.GetMessageBubble(messageSender), chatHolder);
                bubble.SetMessage(message,messageSender);//,GetMessageColor(messageSender));

                if (wantToSendPic)
                {
                    bubble.SetPic(_selectedPic);
                    print("Pic Set");
                    picPanel.SetActive(false);
                    wantToSendPic = false;
                }

                if (BotManager.Instance.isCallingBot)
                {
                    BotManager.Instance.ChekAnswer(message);
                }

                ClearInputField();
                ScrollChat();
                await Task.Yield();
                vlg.enabled = false;
                vlg.enabled = true;
                // bubble.gameObject.SetActive(false);
                // bubble.gameObject.SetActive(true);
                // chatScrollView.gameObject.SetActive(false);
                // chatScrollView.gameObject.SetActive(true);
            }
            
        }


        
         public void BotMessage(BotMessage botMessage)
        {
            var t = 0;
            DOTween.To(() => t, (v) => t = v, 1, botMessage.Delay).OnComplete(async () =>
            {
                var bubble = Instantiate(PrefabHolder.Instance.GetMessageBubble(MessageSender.Character), chatHolder);
                if (botMessage.Message != null)
                {
                    bubble.SetMessage(botMessage.Message,MessageSender.Character);    
                }
                if (botMessage.Image != null)
                {
                    bubble.SetPic(botMessage.Image);
                }

                await Task.Yield();
                // bubble.gameObject.SetActive(false);
                // bubble.gameObject.SetActive(true);
                vlg.enabled = false;
                vlg.enabled = true;
                
                BotManager.Instance.PassNextMessage();
                ScrollChat();
                bubble.CheckContentSizeFitter();
            });
        }

        
        
        
        public void PicPanel(Sprite sprite)
        {
            picPanel.SetActive(true);
            image.sprite = sprite;
            _selectedPic = sprite;
        }

        public async void ScrollChat()
        {
            await Task.Delay(100);
            chatScrollView.verticalNormalizedPosition = 0;
        }


        public void ClearInputField()
        {
            inputField.text = "";
            inputField.text += " ";
        }
        
    }
}