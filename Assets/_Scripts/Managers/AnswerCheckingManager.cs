﻿using System;
using System.Collections.Generic;
using _Scripts.Model;
using _Scripts.Model.ScriptableObjectsSc;
using UnityEngine;
using Utils;

namespace _Scripts.Managers
{
    public class AnswerCheckingManager : MonoBehaviourSingleton<AnswerCheckingManager>
    {
        private SynonymsHolder _synonymsHolder;

        private void Start()
        {
            _synonymsHolder = PrefabHolder.Instance.SynonymsHolder;
        }


        public List<string> CheckForSynonyms(string word)
        {
            var synonyms = _synonymsHolder.Words.Find(x => x.Synonyms1.Contains(word)).Synonyms1;

            return synonyms;
        }


        public bool CheckAnswer(List<string> playerAnswer)
        {
            var correctAnswer = PrefabHolder.Instance.GetAnswers();
            var foundedWord = 0;
            
            foreach (var answer in correctAnswer)
            {
                if (playerAnswer.Contains(answer.Word))
                {
                    foundedWord++;
                }
                else if(answer.HaveSynonym)
                {
                    foreach (var synonym in CheckForSynonyms(answer.Word))
                    {
                        if (playerAnswer.Contains(synonym))
                        {
                            foundedWord++;
                            break;
                        }
                    }
                }
            }

            if (foundedWord >= correctAnswer.Count)
            {
                return true;
            }

            return false;
        }
    }
}