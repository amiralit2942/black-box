﻿using _Scripts.Controller;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using Utils;

namespace _Scripts.Managers
{
    public class PopUpManager : MonoBehaviourSingleton<PopUpManager>
    {
        [SerializeField] private GameObject popUpPanel;
        [SerializeField] private Button acceptBtn;
        [SerializeField] private TMP_Text acceptBtnText;
        [SerializeField] private Button denyBtn;
        [SerializeField] private TMP_Text denyBtnText;
        [SerializeField] private TMP_Text popUpMessage;
        [SerializeField] private TMP_Text popUpHeader;

        public void SpawnPopUp(int buttonNumber)
        {
            popUpPanel.SetActive(true);
            denyBtn.gameObject.SetActive(false);
            
            if (buttonNumber == 2)
            {
                acceptBtn.gameObject.SetActive(true);
            }
            
            var closePopUp = new Button.ButtonClickedEvent();
            closePopUp.AddListener(() =>
            {
                popUpPanel.SetActive(false);
            });
            denyBtn.onClick = closePopUp;
        }

        public void ReportPlayer(ReportButton reportedMessage)
        {
            SpawnPopUp(2);
            popUpMessage.text = "میخوای این پیام رو ریپورت کنی؟";
            acceptBtnText.text = "آره";
            denyBtnText.text = "بیخیال بچه خوبیه";
            
            var report = new Button.ButtonClickedEvent();
            report.AddListener(reportedMessage.Report);
            acceptBtn.onClick = report;
        }

        public void ShowHint(string Hint)
        {
            SpawnPopUp(1);
            popUpHeader.text = "راهنمایی";
            popUpMessage.text = Hint;
            denyBtnText.text = "اوکی";
        }

        public void NoHintLeft()
        {
            SpawnPopUp(1);
            popUpMessage.text = "گفتنیا رو گفتم دیگه چیزی نمونده :)";
            denyBtnText.text = "باش :)";
        }
    }
}
