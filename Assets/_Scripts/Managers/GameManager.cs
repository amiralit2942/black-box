﻿using System;
using _Scripts.Model;
using UnityEngine;
using UnityEngine.UI;
using Utils;

namespace _Scripts.Managers
{
    public class GameManager : MonoBehaviourSingleton<GameManager>
    {
        private int _gameState;
        public int GameState => _gameState;
        public Image backGround;

        private void Awake()
        {
            _gameState = 0;
        }

        public void NextState()
        {
            _gameState++;
            BotManager.Instance.PassNextStateAnswers();
            var sprite = PrefabHolder.Instance.GetBackGroundSprite();
            if (sprite != null)
            {
                //Change The Background
            }
            //TODO Change BackGround
        }
        
        
    }
}