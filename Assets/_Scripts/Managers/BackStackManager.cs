﻿using System;
using System.Collections.Generic;
using _Scripts.Utils.Animating_Utils;
using UnityEngine;
using Utils;

namespace _Scripts.Managers
{
    public class BackStackManager : MonoBehaviourSingleton<BackStackManager>
    {
        private Stack<AnimatedPanel> _panelsStack;
        
        void Awake()
        {
            DontDestroyOnLoad(this);
            _panelsStack = new Stack<AnimatedPanel>();
        }

        private void Update()
        {
            if (Input.GetKeyUp(KeyCode.Escape))
            {
                //TODO : IF ANY POPUP IS ACTIVE , DeACTIVE THEM FIRST
                Pop();
            }
        }

        public void Pop()
        {
            if (_panelsStack.Count > 0)
            {
                _panelsStack.Peek().Animate(true);
                _panelsStack.Pop();
                if (_panelsStack.Count > 0)
                {
                    _panelsStack.Peek().Animate(false);    
                }
            }
            else
            {
                print("Exited");
                //TODO: Show Exit Panel
            }
            
        }

        public void Push(AnimatedPanel panel)
        {
            if (_panelsStack.Count > 0)
            {
                if (_panelsStack.Peek() == panel)
                {
                    return;
                }    
            }
            panel.Animate(false);
            _panelsStack.Push(panel);
        }

    }
}
