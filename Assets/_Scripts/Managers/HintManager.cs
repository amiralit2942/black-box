using _Scripts.Model;
using Utils;

namespace _Scripts.Managers
{
    public class HintManager : MonoBehaviourSingleton<HintManager>
    {
        private int _hints;
        public int Hints => _hints;

        private void Start()
        {
            //Get Hints From Safe Box
        }

        public void UseHints()
        {
            if (_hints > 0)
            {
                //Use
                var hint = PrefabHolder.Instance.GetHint();
                if (hint != null)
                {
                    // Decrease one Hint And Show one
                    PopUpManager.Instance.ShowHint(hint);
                }
                else
                {
                    //Show No Hints Left Panel
                    PopUpManager.Instance.NoHintLeft();
                }
            }
            else
            {
                //Buy Hints
            }
        }

        public void EarnHint(int earnedHint)
        {
            //Add Hints   
        }
    }
}
