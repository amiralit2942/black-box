﻿using UnityEngine.SceneManagement;
using Utils;

namespace _Scripts.Managers
{
    public class ButtonManager : MonoBehaviourSingleton<ButtonManager>
    {
        public void ProfileButton()
        {
            CanvasManager.Instance.OpenProfile();
        }

        public void TellUsButton()
        {
            CanvasManager.Instance.OpenTellUs();
        }

        public void GameInfoButton()
        {
            CanvasManager.Instance.OpenGameInfo();
        }

        public void OpenFirstGame()
        {
            SceneManager.LoadScene(1);
        }
        
        
    }
}