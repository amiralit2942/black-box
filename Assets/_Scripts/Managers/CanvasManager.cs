﻿using _Scripts.Utils.Animating_Utils;
using UnityEngine;
using Utils;

namespace _Scripts.Managers
{
    public class CanvasManager : MonoBehaviourSingleton<CanvasManager>
    {
        [SerializeField] private AnimatedPanel menuPanel;
        [SerializeField] private AnimatedPanel profilePanel;
        [SerializeField] private AnimatedPanel tellUsPanel;
        [SerializeField] private AnimatedPanel gameInfoPanel;


        private void Start()
        {
            //OpenMenu();
        }

        public void OpenMenu()
        {
            DisablePanels();
            //menuPanel.SetActive(true);
            BackStackManager.Instance.Push(menuPanel);
        }

        public void OpenProfile()
        {
            DisablePanels();
           // profilePanel.SetActive(true);
            BackStackManager.Instance.Push(profilePanel);
        }

        public void OpenTellUs()
        {
            DisablePanels();
            //tellUsPanel.SetActive(true);
            BackStackManager.Instance.Push(tellUsPanel);
        }

        public void OpenGameInfo()
        {
            DisablePanels();
            //gameInfoPanel.SetActive(true);
            BackStackManager.Instance.Push(gameInfoPanel);
        }
        
        
        public void DisablePanels()
        {
            // menuPanel.SetActive(false);
            // profilePanel.SetActive(false);
            // tellUsPanel.SetActive(false);
            // gameInfoPanel.SetActive(false);
        }
    }
}
