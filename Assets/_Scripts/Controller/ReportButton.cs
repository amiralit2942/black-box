﻿using System;
using _Scripts.Managers;
using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;

namespace _Scripts.Controller
{
    public class ReportButton : MonoBehaviour
    {
        [SerializeField] private float duration;
        [SerializeField] private RectTransform reportBtn;
        [SerializeField] private Vector2 reportBtnSize;
        [SerializeField] private RectTransform openButton;
        [SerializeField] private Vector2 openBtnSize;
        [SerializeField] private RectTransform closeButton;

        private void Awake()
        {
            reportBtn.gameObject.SetActive(false);
            openButton.gameObject.SetActive(false);
            closeButton.gameObject.SetActive(false);
        }

        private void OnEnable()
        {
            ChatBroadCastManager.Instance.CloseReportButtons += Close;
        }
        

        private bool _isOpen = false;

        public void OpenPanel()
        {
            ChatBroadCastManager.Instance.CloseReportButtons?.Invoke();
            closeButton.gameObject.SetActive(true);
            openButton.transform.DOComplete();
            openButton.DOSizeDelta(new Vector2(), duration).OnComplete(() =>
            {
                openButton.gameObject.SetActive(false);
                reportBtn.gameObject.SetActive(true);
                reportBtn.DOComplete();
                reportBtn.DOSizeDelta(reportBtnSize, duration);
            });
            _isOpen = true;

            // else
            // {
            //     closeButton.gameObject.SetActive(false);
            //     
            //     reportBtn.DOComplete();
            //     reportBtn.DOSizeDelta(new Vector2(), duration).OnComplete(() =>
            //     {
            //         reportBtn.gameObject.SetActive(false);
            //         openButton.gameObject.SetActive(true);
            //         openButton.DOComplete();
            //         openButton.DOSizeDelta(openBtnSize, duration);
            //     });
            //     _isOpen = false;
            // }
        }

        public void Close()
        {
            closeButton.gameObject.SetActive(false);

            reportBtn.DOComplete();
            reportBtn.DOSizeDelta(new Vector2(), duration).OnComplete(() =>
            {
                reportBtn.gameObject.SetActive(false);
                openButton.gameObject.SetActive(true);
                openButton.DOComplete();
                openButton.DOSizeDelta(openBtnSize, duration);
            });
            _isOpen = false;
        }

        public void ReportPanel()
        {
            PopUpManager.Instance.ReportPlayer(this);
        }

        public void Report()
        {
            print("Player Reported");
        }

        public void CanReport()
        {
            openButton.gameObject.SetActive(true);
        }
    }
}