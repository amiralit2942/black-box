﻿using System;
using _Scripts.Managers;
using _Scripts.Model.ScriptableObjectsSc;
using _Scripts.Utils.Animating_Utils;
using DG.Tweening;
using UnityEngine;

namespace _Scripts.Controller.KeyBoard
{
    public class KeyBoard : AnimatedPanel
    {
        [SerializeField] private GameObject numbersPanel;
        [SerializeField] private GameObject alphabetPanel;
        [SerializeField] private RectTransform keyBoardRT;
        [SerializeField] private RectTransform view;
        [SerializeField] private GameObject openKeyBoardBtn;
        [SerializeField] private float openingDuration;
        [SerializeField] private int viewOpenSize;
        [SerializeField] private int viewCloseSize;
        private bool _keyboardIsOpen = true;
        private void Start()
        {
            AlphabetPanel();
            //OpenKeyBoard();
            Animate(true);
        }

        private void Update()
        {
            // if (Input.GetKeyDown(KeyCode.Escape))
            // {
            //     if (_keyboardIsOpen)
            //     {
            //         OpenKeyBoard();    
            //     }
            // }
        }

        public void BackSpace()
        {
            //TODO NOT FUCKING WORKING
            ChatManager.Instance.inputField.text =
                ChatManager.Instance.inputField.text.Remove(ChatManager.Instance.inputField.text.Length - 1 , 1);
        }

        public void Send()
        {
            ChatManager.Instance.
                Message(ChatManager.Instance.inputField.text,MessageSender.Player);
        }

        public void NumbersPanel()
        {
            alphabetPanel.SetActive(false);
            numbersPanel.SetActive(true);    
        }
    
        public void AlphabetPanel()
        {
            alphabetPanel.SetActive(true);
            numbersPanel.SetActive(false);    
        }
        
        public void Space()
        {
            ChatManager.Instance.inputField.text += " ";
        }

        public void NewLine()
        {
            ChatManager.Instance.inputField.text += "\n";
        }

        
        public void OpenKeyBoard()
        {
            // if (_keyboardIsOpen)
            // {
            //     //Close
            //     // keyBoard.DOComplete();
            //     // keyBoard.DOAnchorMin(new Vector2(.5f, 0), .01f);
            //     // keyBoard.DOAnchorMax(new Vector2(.5f, .3f), .01f);
            //     // keyBoard.DOAnchorPosY(0, openingDuration);
            //     //keyBoard.DOLocalMove(new Vector2(0, -1360), openingDuration);
            //     // keyBoardRT.DOComplete();
            //     // keyBoardRT.DOPivotY(1, openingDuration).OnUpdate(() =>
            //     // {
            //     //     keyBoardRT.anchoredPosition = Vector2.zero;
            //     // }).OnComplete(() =>
            //     // {
            //     //     keyBoardRT.anchoredPosition = Vector2.zero;
            //     // });
            //     //
            //     // view.DOComplete(); 
            //     // view.DOSizeDelta(new Vector2(view.sizeDelta.x, 100), openingDuration);
            //     Animate(true);
            //     openKeyBoardBtn.SetActive(true);
            //     _keyboardIsOpen = false;
            //     
            // }
            // else
            // {
            //     //Open
            //     // keyBoard.DOComplete();
            //     // keyBoard.DOLocalMove(new Vector2(), openingDuration);
            //     // keyBoardRT.DOComplete();
            //     // keyBoardRT.DOPivotY(0, openingDuration).OnUpdate(() =>
            //     // {
            //     //     keyBoardRT.anchoredPosition = Vector2.zero;
            //     // }).OnComplete(() =>
            //     // {
            //     //     keyBoardRT.anchoredPosition = Vector2.zero;
            //     // });
            //     // view.DOComplete(); //-1208
            //     // view.DOSizeDelta(new Vector2(view.sizeDelta.x, -1200), openingDuration); 
            //     //ChatManager.Instance.ScrollChat();
            //     Animate(false);
            //     openKeyBoardBtn.SetActive(false);
            //     _keyboardIsOpen = true;
            //     
            // }
            BackStackManager.Instance.Push(this);
        }

        public override void Animate(bool disabling = false)
        {
            if (!disabling)
            {
                //Open
                keyBoardRT.DOComplete();
                keyBoardRT.DOPivotY(0, openingDuration).OnUpdate(() =>
                {
                    keyBoardRT.anchoredPosition = Vector2.zero;
                }).OnComplete(() =>
                {
                    keyBoardRT.anchoredPosition = Vector2.zero;
                }).SetEase(enableAnimationOptions.animationCurve);
                view.DOComplete(); //-1208
                view.DOSizeDelta(new Vector2(view.sizeDelta.x, -1200), openingDuration)
                    .SetEase(enableAnimationOptions.animationCurve);
                openKeyBoardBtn.gameObject.SetActive(false);
            }
            else
            {
                //Close
                keyBoardRT.DOComplete();
                keyBoardRT.DOPivotY(1, openingDuration).OnUpdate(() =>
                {
                    keyBoardRT.anchoredPosition = Vector2.zero;
                }).OnComplete(() =>
                {
                    keyBoardRT.anchoredPosition = Vector2.zero;
                }).SetEase(disableAnimationOptions.animationCurve);

                view.DOComplete(); 
                view.DOSizeDelta(new Vector2(view.sizeDelta.x, 83), openingDuration)
                    .SetEase(disableAnimationOptions.animationCurve);
                openKeyBoardBtn.gameObject.SetActive(true);
            }
        }
        
    }
}
