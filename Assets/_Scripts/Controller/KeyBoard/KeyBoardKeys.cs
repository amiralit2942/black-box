﻿using System;
using _Scripts.Managers;
using TMPro;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace _Scripts.Controller
{
    public class KeyBoardKeys : MonoBehaviour , IPointerDownHandler,IPointerUpHandler
    {
        [SerializeField] public Button key;
        [SerializeField] public char character;
        [SerializeField] public TMP_Text buttonText;
        [SerializeField] public Image image;
        public Color pressedColor;

        private void Start()
        {
            buttonText.text = character.ToString();
        }

        public void OnMouseDown()
        {
            ChatManager.Instance.inputField.text += character;
            key.image.color = pressedColor;
        }


        public void OnPointerDown(PointerEventData eventData)
        {
            ChatManager.Instance.inputField.text += character;
            key.image.color = pressedColor;
        }

        public void OnPointerUp(PointerEventData eventData)
        {
            key.image.color = Color.white;
        }

        
    }
}
