using System;
using UnityEngine;
using UnityEngine.UI;

namespace _Scripts.Controller.Menu
{
    public class ButtonAndContent : MonoBehaviour
    {
        [SerializeField] private Transform content;
        public Transform Content => content;
        [SerializeField] private Button button;
        public Button Button => button;


        public void Click()
        {
            GamesSliderView.Instance.ButtonClick(this);
        }
    }
}
