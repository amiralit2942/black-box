using System;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;

namespace _Scripts.Utils.Animating_Utils
{
    public class ButtonHollowAnim : MonoBehaviour
    {
        [SerializeField] private float duration;
        [SerializeField] private List<Image> images;
        
        void Start()
        {
            if (images != null)
            {
                var tempColor = images[0].color;
                tempColor.a = 156 / 255f;
                images[0].color = tempColor;
                images.Remove(images[0]);
                HollowAnim();   
            }
        }

        private void HollowAnim()
        {
            images.ForEach(x=>x.DOFade(0f, duration).SetLoops(-1, LoopType.Yoyo));
        }
    }
}