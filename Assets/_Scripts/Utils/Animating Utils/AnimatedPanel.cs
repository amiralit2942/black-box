﻿using System;
using DG.Tweening;
using UnityEngine;

namespace _Scripts.Utils.Animating_Utils
{
	[Serializable]
	public class AnimatedPanelOptions
	{
		public float duration;
		public bool animatePosition;
		public Vector2 positionDisableValue;
		public Vector2 positionEnableValue;
		public bool animateSizeDelta;
		public Vector2 sizeDisableValue;
		public Vector2 sizeEnableValue;
		public bool animateTransparency;
		public bool useAnimationCurve;
		public AnimationCurve animationCurve;
	}
	
	public class AnimatedPanel : MonoBehaviour
	{
		[SerializeField] private RectTransform panel;
		[SerializeField] protected AnimatedPanelOptions enableAnimationOptions;
		[SerializeField] protected AnimatedPanelOptions disableAnimationOptions;
		[SerializeField] private bool animateOnDisable;
		private CanvasGroup _canvasGroup;

		private void FillCanvasGroup()
		{
			if (_canvasGroup != null) return;
			_canvasGroup = gameObject.GetComponent<CanvasGroup>();
			if (_canvasGroup != null) return;
			_canvasGroup = gameObject.AddComponent<CanvasGroup>();
		}
		
		private void OnEnable()
		{
			Animate();
		}

		public void Disable()
		{
			if (!animateOnDisable)
			{
				gameObject.SetActive(false);
				return;
			}
			Animate(true);
		}

		public virtual void Animate(bool disabling = false)
		{
			if (!disabling)
			{
				//Open
				if (enableAnimationOptions.animatePosition)
				{
					panel.anchoredPosition = enableAnimationOptions.positionDisableValue;
					if (enableAnimationOptions.useAnimationCurve)
					{
						panel.DOAnchorPos(enableAnimationOptions.positionEnableValue, enableAnimationOptions.duration)
							.SetEase(enableAnimationOptions.animationCurve);
					}
					else
					{
						panel.DOAnchorPos(enableAnimationOptions.positionEnableValue, enableAnimationOptions.duration);
					}
					
				}

				if (enableAnimationOptions.animateSizeDelta)
				{
					panel.sizeDelta = enableAnimationOptions.sizeDisableValue;
					if (enableAnimationOptions.useAnimationCurve)
					{
						panel.DOSizeDelta(enableAnimationOptions.sizeEnableValue, enableAnimationOptions.duration)
							.SetEase(enableAnimationOptions.animationCurve);
					}
					else
					{
						panel.DOSizeDelta(enableAnimationOptions.sizeEnableValue, enableAnimationOptions.duration);
					}
				}

				if (enableAnimationOptions.animateTransparency)
				{
					FillCanvasGroup();
					_canvasGroup.alpha = 0;
					_canvasGroup.DOFade(1, enableAnimationOptions.duration);
				}
				else
				{
					FillCanvasGroup();
					_canvasGroup.alpha = 1;
				}
			}
			else
			{
				//Close
				
				// TODO : Use Animation curve on disable animations
				if (disableAnimationOptions.animatePosition)
				{
					panel.anchoredPosition = disableAnimationOptions.positionEnableValue;
					panel.DOAnchorPos(disableAnimationOptions.positionDisableValue, disableAnimationOptions.duration).OnComplete(() =>
					{
						gameObject.SetActive(false);
					});
				}

				if (disableAnimationOptions.animateSizeDelta)
				{
					panel.sizeDelta = disableAnimationOptions.sizeEnableValue;
					panel.DOSizeDelta(disableAnimationOptions.sizeDisableValue, disableAnimationOptions.duration).OnComplete(() =>
					{
						gameObject.SetActive(false);
					});
				}

				if (disableAnimationOptions.animateTransparency)
				{
					FillCanvasGroup();
					_canvasGroup.alpha = 1;
					_canvasGroup.DOFade(0, disableAnimationOptions.duration).OnComplete(() =>
					{
						gameObject.SetActive(false);
					});
				}
				else
				{
					FillCanvasGroup();
					_canvasGroup.alpha = 0;
				}
			}
		}
	}
}

