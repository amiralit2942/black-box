using System;
using DG.Tweening;
using UnityEngine;

namespace _Scripts.Utils.Animating_Utils
{
    public class UpAndDownAnim : MonoBehaviour
    {
        [SerializeField] private RectTransform rectTransform;
        [SerializeField] private float movingDiff;
        [SerializeField] private float duration;
        private void Start()
        {
            Move();
        }

        public void Move()
        {
            transform.DOMoveY(transform.position.y - movingDiff,duration)
                .SetLoops(-1, LoopType.Yoyo);
        }
    }
}
