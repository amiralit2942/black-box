using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class TwoSideScrollView : MonoBehaviour,IBeginDragHandler,IEndDragHandler,IDragHandler
{
    [SerializeField] private ScrollRect _sourceScrollRect;
    [SerializeField] private ScrollRect _scrollRect;
    private Vector2 _firstPosition;
    private bool _isAllowDrag;
    
    public void OnBeginDrag(PointerEventData eventData)
    {
        if (Mathf.Abs(eventData.delta.x) >Mathf.Abs( eventData.delta.y))
        {
            _sourceScrollRect.OnBeginDrag(eventData);  
            _scrollRect.vertical = false;
            _isAllowDrag = false;
            _firstPosition = eventData.position;
        }
        else
        {
            _isAllowDrag = true;
            _scrollRect.vertical = true;
        }
    }

    public void OnEndDrag(PointerEventData eventData)
    {
        if (!_isAllowDrag)
        {
         //   _sourceScrollRect.OnEndDrag(eventData);
         var diff = eventData.position - _firstPosition;
            _isAllowDrag = true;
            GamesSliderView.Instance.SetChildToCenter(diff);
        }
        _scrollRect.vertical = true;
    }

    public void OnDrag(PointerEventData eventData)
    {
        if (!_isAllowDrag)
        {
            _sourceScrollRect.OnDrag(eventData);            
        }
    }
}
