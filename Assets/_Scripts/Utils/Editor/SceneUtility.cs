using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public class SceneUtility : MonoBehaviour
{
    [MenuItem("Scenes/Menu")]
    public static void OpenMenu()
    {
        var path = EditorBuildSettings.scenes[0].path;
        EditorApplication.SaveCurrentSceneIfUserWantsTo();
        EditorApplication.OpenScene(path);
    }

    [MenuItem("Scenes/GamePlay")]
    public static void OpenGameplay()
    {
        var path = EditorBuildSettings.scenes[1].path;
        EditorApplication.SaveCurrentSceneIfUserWantsTo();
        EditorApplication.OpenScene(path);
    }
    
}
