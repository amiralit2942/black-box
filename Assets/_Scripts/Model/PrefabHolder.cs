﻿using System;
using System.Collections.Generic;
using _Scripts.Managers;
using _Scripts.Model.Chat;
using _Scripts.Model.ScriptableObjectsSc;
using UnityEngine;
using UnityEngine.XR;
using Utils;
using Random = UnityEngine.Random;

namespace _Scripts.Model
{
    public class PrefabHolder : MonoBehaviourSingleton<PrefabHolder>
    {
        [SerializeField] private QAndAHolder qAndaHolder;
        [SerializeField] private ChatBubbleHolder chatBubbleHolder;
        [SerializeField] private HandyResponsesHolder handyResponses;
        [SerializeField] private  BackGroundHolder backGrounds;
        [SerializeField] private SynonymsHolder _synonymsHolder;
        
        
        public SynonymsHolder SynonymsHolder => _synonymsHolder;


        private void Awake()
        {
            DontDestroyOnLoad(this);
        }


        public List<KeyWords> GetAnswers()
        {
            var answers =
                qAndaHolder.QAndA.Find(x => x.GameState == GameManager.Instance.GameState).KeyWords;
            return answers;
        }

        public List<BotMessage> PassMessages()
        {
            return qAndaHolder.QAndA.Find
                (x => x.GameState == GameManager.Instance.GameState).BotMessages;
        }

        public string GetHint()
        {
            var hints = qAndaHolder.QAndA.Find(x => x.GameState == GameManager.Instance.GameState).Hints;
            foreach (var hint in hints)
            {
                if (!hint.Used)
                {
                    return hint.Hint;
                }
            }

            return null;
        }
        
        public MessageBubble GetMessageBubble(MessageSender messageSender)
        {
            return chatBubbleHolder.ChatBubbles.Find(x => 
                x.MessageSender == messageSender).MessageBubble;
        }

        public string GetRandomHandyResponse(HandyResponseType responseType)
        {
            var handyResponsesList = qAndaHolder.QAndA.Find(x => x.GameState == GameManager.Instance.GameState).HandyResponses;
            var appropriateResponses = handyResponsesList.Find(x => x.ResponseType == responseType).Response;
            return appropriateResponses[Random.Range(0, appropriateResponses.Count)];
        }

        public Sprite GetBackGroundSprite()
        {
           var sprite = backGrounds.BackGrounds
               ?.Find(x => x.GameState == GameManager.Instance.GameState).Sprite;

           return sprite;
        }
        
        
    }
}