﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace _Scripts.Model.ScriptableObjectsSc
{
    public enum HandyResponseType
    {
        DontDisturb,
        WrongAnswer,
        CallBot
    }
    
    [Serializable]
    public class HandyResponses
    {
        [SerializeField] private HandyResponseType responseType;
        public HandyResponseType ResponseType => responseType;
        
        [SerializeField] private List<string> response;
        public List<string> Response => response;
    }
    
    [CreateAssetMenu(fileName = "HandyResponses",menuName = "ScriptableObject/HandyResponses",order = 3)]
    public class HandyResponsesHolder : ScriptableObject
    {
        [SerializeField] private List<HandyResponses> handyResponses;
        public List<HandyResponses> HandyResponses => handyResponses;
    }
}
