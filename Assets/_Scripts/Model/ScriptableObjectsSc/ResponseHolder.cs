﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace _Scripts.Model.ScriptableObjectsSc
{
    
        [Serializable]
        public class Response
        {
            [SerializeField] private int gameState;
            public int GameState => gameState;
            
            [SerializeField] private List<string> answers;
            public List<string> Answers => answers;
        }

        // [Serializable]
        // public class Message
        // {
        //     [SerializeField] private string message1;
        //     public string Message1 => message1;
        // }

        [CreateAssetMenu(fileName = "ResponseHolder", menuName = "ScriptableObject/ResponseHolder", order = 2)]
        public class ResponseHolder : ScriptableObject
        {
            [SerializeField] private List<Response> responsesList;
            public List<Response> ResponsesList => responsesList;
        }
    
}
