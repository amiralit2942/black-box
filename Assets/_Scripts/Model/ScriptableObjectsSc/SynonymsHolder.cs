﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace _Scripts.Model.ScriptableObjectsSc
{

    [Serializable]
    public class Synonyms
    {
        [SerializeField] private List<string> _synonyms;

        public List<string> Synonyms1 => _synonyms;
    }
    
    
    [CreateAssetMenu(fileName = "SynonymsHolder", menuName = "ScriptableObject/SynonymsHolder", order = 15)]
    public class SynonymsHolder : ScriptableObject
    {
        [SerializeField] private List<Synonyms> _words;
        public List<Synonyms> Words => _words;
    }
}