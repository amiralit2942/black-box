﻿using System;
using System.Collections.Generic;
using _Scripts.Managers;
using UnityEngine;
using UnityEngine.UI;

namespace _Scripts.Model.ScriptableObjectsSc
{

    public enum MessageSender
    {
        Player,
        Teammates,
        Character,
        Manager
    }
    [Serializable]
    public class MessageInfo
    {
        [SerializeField] private string message;
        [SerializeField] private Sprite image;
        [SerializeField] private int gameState;
        [SerializeField] private int order;
        [SerializeField] private float messageDelay;
        [SerializeField] private MessageSender messageSender;
        public string Message => message;
        public Sprite Image => image;
        public int GameState => gameState;
        public int Order => order;
        public float MessageDelay => messageDelay;
        public MessageSender MessageSender => messageSender;
    }
    
    [CreateAssetMenu(fileName = "MessageInfoHolder",menuName = "ScriptableObject/MessageInfoHolder",order = 4)]
    public class MessageInfoHolder : ScriptableObject
    {
         [SerializeField] private List<MessageInfo> messageInfos;
        public List<MessageInfo> MessageInfos =>  messageInfos;
    }
}
