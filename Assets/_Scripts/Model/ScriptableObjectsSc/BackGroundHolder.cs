﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace _Scripts.Model.ScriptableObjectsSc
{
    [CreateAssetMenu(fileName = "BackGroundInfo",menuName = "BackGroundInfo/ScriptableObject",order = 6)]
    public class BackGroundHolder : ScriptableObject
    {
        [SerializeField] private List<BackGroundInfo> backGrounds;
        public List<BackGroundInfo> BackGrounds => backGrounds;
    }

    [Serializable]
    public class BackGroundInfo
    {
        [SerializeField] private int gameState;
        public int GameState => gameState;
        [SerializeField] private Sprite sprite;
        public Sprite Sprite => sprite;
    }
    
}
