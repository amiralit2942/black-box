using System;
using System.Collections.Generic;
using _Scripts.Model.Chat;
using UnityEngine;

namespace _Scripts.Model.ScriptableObjectsSc
{

    [Serializable]
    public class ChatBubblePrefab
    {
        [SerializeField] private MessageBubble messageBubble;
        [SerializeField] private MessageSender messageSender;

        public MessageBubble MessageBubble => messageBubble;
        public MessageSender MessageSender => messageSender;
    }


    [CreateAssetMenu(fileName = "ChatBubbleHolder",menuName = "ScriptableObject/ChatBubbleHolder")]

    public class ChatBubbleHolder : ScriptableObject
    {
        [SerializeField] private List<ChatBubblePrefab> chatBubbles;
        public List<ChatBubblePrefab> ChatBubbles => chatBubbles;
    }
}
