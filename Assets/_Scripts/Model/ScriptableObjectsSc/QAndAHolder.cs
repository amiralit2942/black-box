﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace _Scripts.Model.ScriptableObjectsSc
{
    [Serializable]
    public class BotMessage
    {
        
        [SerializeField] private int order;
        [SerializeField] private MessageSender messageSender;
        [SerializeField] private int delay;
        [SerializeField] private string message;
        [SerializeField] private Sprite image;
        [SerializeField] private bool botIsBusy;
        
        public int Order => order;
        public MessageSender MessageSender => messageSender;
        public int Delay => delay;
        public string Message => message;
        public Sprite Image => image;
        public bool BotIsBusy => botIsBusy;
    }
    
    [Serializable]
    public class HandHandyResponse
    {
        [SerializeField] private HandyResponseType responseType;
        public HandyResponseType ResponseType => responseType;
        
        [SerializeField] private List<string> response;
        public List<string> Response => response;
    }

    [Serializable]
    public class KeyWords
    {
        [SerializeField] private string word;
        [SerializeField] private bool haveSynonym;
        public string Word => word;
        public bool HaveSynonym => haveSynonym;
    }

    [Serializable]
    public class HintHolder
    {
        [SerializeField] private string hint;
        public string Hint => hint;
        [SerializeField] private bool _used;
        public bool Used => _used;
    }

    [Serializable]
    public class QAndA
    {
        [SerializeField] private int gameState;
        [SerializeField] private List<BotMessage> botMessages;
        [SerializeField] private List<KeyWords> keyWords;
        [SerializeField] private List<HintHolder> hints;
        [SerializeField] private List<HandHandyResponse> handyResponses;
        [SerializeField] private Sprite sprite;
        
        public int GameState => gameState;
        public List<BotMessage> BotMessages => botMessages;
        public List<KeyWords> KeyWords => keyWords;
        public List<HintHolder> Hints => hints;
        public List<HandHandyResponse> HandyResponses => handyResponses;
        public Sprite Sprite => sprite;
    } 

    [CreateAssetMenu(fileName = "QuestionAndAnswer", menuName = "ScriptableObject/QuestionAndAnswer", order = 0)]
    public class QAndAHolder : ScriptableObject
    {
        [SerializeField] private List<QAndA> qAndA;
        public List<QAndA> QAndA => qAndA;
    } 
}