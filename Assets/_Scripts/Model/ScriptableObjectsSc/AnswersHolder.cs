﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace _Scripts.Model.ScriptableObjectsSc
{
    
        [Serializable]
        public class Answer
        {
            [SerializeField] private int gameState;
            public int GameState => gameState;
            [SerializeField] private List<string> answer;
            public List<string> GETAnswer => answer;
        }

        [CreateAssetMenu(fileName = "AnswerHolder", menuName = "ScriptableObject/AnswerHolder", order = 1)]
        public class AnswerHolder : ScriptableObject
        {
            [SerializeField] private List<Answer> answers;
            public List<Answer> Answers => answers;
        }
    
}
