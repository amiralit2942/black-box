﻿using System;
using System.Net.Mime;
using System.Threading.Tasks;
using _Scripts.Controller;
using _Scripts.Managers;
using _Scripts.Model.ScriptableObjectsSc;
using DG.Tweening;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace _Scripts.Model.Chat
{
    public class MessageBubble : MonoBehaviour
    {
        [SerializeField] private TMP_Text senderName;
        [SerializeField] private TMP_Text messageText;
        [SerializeField] public Image image;
        [SerializeField] private VerticalLayoutGroup vlg;
        private ReportButton _reportButton;
        public MessageSender _messageSender;
        
        private void Start()
        {
            _reportButton = GetComponent<ReportButton>();
            if (_messageSender == MessageSender.Teammates || _messageSender == MessageSender.Player)
            {
                _reportButton.CanReport();
            }
        }

        public void SetMessage(string message, MessageSender messageSender)//,Color color)
        {
            _messageSender = messageSender;
            switch (messageSender)
            {
                case MessageSender.Player:
                    senderName.text = "من";
                    break;
                case MessageSender.Manager:
                    senderName.text = "مدیر";
                    break;
                case MessageSender.Character:
                    senderName.text = "کاراکتر بازی";
                    break;
                case MessageSender.Teammates:
                    senderName.text = "هم تیمی";
                    break;
            }
            messageText.text = message;
            // messageText.color = color;
        }

        public void SetPic(Sprite sprite)
        {
            image.gameObject.SetActive(true);
            image.sprite = sprite;
        }

        async public void CheckContentSizeFitter()
        {
            vlg.enabled = false;
            await Task.Yield();
            vlg.enabled = true;
        }
        
        
        
    }
}