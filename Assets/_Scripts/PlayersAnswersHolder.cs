﻿using System.Collections.Generic;
using UnityEngine;

namespace _Scripts
{
    [CreateAssetMenu(fileName = "PlayersAnswersHolder", menuName = "ScriptableObject/PlayersAnswersHolder", order = 0)]
    public class PlayersAnswersHolder : ScriptableObject
    {
        private List<string> _answers;
        public List<string> Answers => _answers;
    }
}